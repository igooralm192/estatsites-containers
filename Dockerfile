FROM igorneodados/estatsites:1.1

ARG APP_NAME

WORKDIR /var/www/${APP_NAME}

RUN mv ../drupal.tar.gz .

RUN tar -xz --strip-components=1 -f drupal.tar.gz; \
	rm drupal.tar.gz; \
	chown -R www-data:www-data sites modules themes

ARG APACHE_CONF_DIR
ARG PHP_CONF_DIR
ARG APP_NAME
ARG APP_PORT

COPY config/apache2.conf ${APACHE_CONF_DIR}/apache2.conf
COPY config/app.conf ${APACHE_CONF_DIR}/sites-enabled/${APP_NAME}.conf
COPY config/php.ini  ${PHP_CONF_DIR}/apache2/conf.d/custom.ini

COPY app/ .

EXPOSE ${APP_PORT}

CMD ["/sbin/entrypoint.sh"]